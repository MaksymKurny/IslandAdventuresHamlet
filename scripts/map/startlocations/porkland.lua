local STRINGS = GLOBAL.STRINGS

AddStartLocation("PorklandStart", {
    name = STRINGS.UI.SANDBOXMENU.SHIPWRECKED,
    location = "forest",
    start_setpeice = "PorklandStart",
    start_node = "BG_rainforest_base"
})
