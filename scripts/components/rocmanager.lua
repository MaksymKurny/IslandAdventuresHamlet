
local SPAWNDIST = 40
local TESTTIME = TUNING.SEG_TIME/2

local RocManager = Class(function(self, inst)
	self.disabled = false
	self.inst = inst
	self.inst:DoPeriodicTask(TESTTIME,function() self:ShouldSpawn() end) -- 
	self.roc = nil
	self.nexttime = self:GetNextSpawnTime()
	self.inst:ListenForEvent("master_clockupdate")
end)

function RocManager:OnSave()	
	local refs = {}
	local data = {}
	data.disabled = self.disabled
	data.nexttime = self.nexttime
	return data, refs
end 

function RocManager:OnLoad(data)

	if data.disabled then
		self.disabled = data.disabled
	end
	if data.nexttime then
		self.nexttime = data.nexttime
	end
end
function RocManager:RemoveRoc(inst)
	if self.roc == inst then 
		self.roc = nil
	end
end

function RocManager:GetNextSpawnTime()
	return (TUNING.TOTAL_DAY_TIME * 10)  + (math.random() * TUNING.TOTAL_DAY_TIME * 10)
end

function RocManager:Spawn()
	if not self.roc then
		local pt= Vector3(GetPlayer().Transform:GetWorldPosition())
		local angle = math.random()* 2*PI
		local offset = Vector3(SPAWNDIST * math.cos( angle ), 0, -SPAWNDIST * math.sin( angle ))
		local roc = SpawnPrefab("roc")
		roc.Transform:SetPosition(pt.x+offset.x,0,pt.z+offset.z)
		self.roc = roc
		self.nexttime = self:GetNextSpawnTime()			
	end
end

function RocManager:ShouldSpawn()
	if self.disabled then
		return
	end

	if self.nexttime > 0 then
		self.nexttime = self.nexttime - TESTTIME
	end	
	-- will only spawn before the first half of daylight, and not wile player is indoors
	--asgerrr: TheWorld.state.timeinphase = the portion of the phase that has already passed
	if not self.roc and TheWorld.state.isday and TheWorld.state.timeinphase < 0.5 and not TheCamera.interior then 
		-- do test stuff.
		if self.nexttime <= 0 then
			self:Spawn()
		end		
	end
end

function RocManager:LongUpdate(dt)
	self.nexttime = self.nexttime - dt
end

function RocManager:OnUpdate(dt)

end

return RocManager