local STRINGS = GLOBAL.STRINGS

local speech = {
	-- "generic",
	-- "willow",
	-- "wolfgang",
	"wendy",
	-- "wx78",
	-- "wickerbottom",
	-- "woodie",
	-- "wes",
	-- "waxwell",
	-- "wathgrithr",
	-- "webber",
	-- "winona",
	-- "wortox",
	-- "wormwood",
	-- "warly",
	-- "wurt",
	-- "walter",
	"wanda",
}

local newspeech = {
	-- "walani",
	-- "wilbur",
	-- "woodlegs",
}

local pigstuff = {
	"pig_names",
	"pig_speech"
}

local other = {
	--"common" is what IA-SW calls the file with all the non-speech strings, mostly being UI stuff
	"common"
}

local function merge(target, new, soft)
	if not target then
		target = {}
	end

	for k, v in pairs(new) do
		if type(v) == "table" then
			target[k] = type(target[k]) == "table" and target[k] or {}
			merge(target[k], v)
		else
			if target[k] then
				if soft then
					-- print("couldn't add " ..  k, " (already is \"" ..  target[k]  .. "\")")
				else
					-- print("replacing " ..  k, " (with \"" ..  v  .. "\")")
					target[k] = v
				end
			else
				target[k] = v
			end
		end
	end
	return target
end

-- Install our crazy loader!
local function import(modulename)
	print("modimport (strings file): " .. MODROOT .. "strings/" .. modulename)
	-- if string.sub(modulename, #modulename-3,#modulename) ~= ".lua" then
		-- modulename = modulename .. ".lua"
	-- end
	local result = kleiloadlua(MODROOT .. "strings/" .. modulename)
	if result == nil then
		error("Error in custom import: Stringsfile " .. modulename .. " not found!")
	elseif type(result) == "string" then
		error("Error in custom import: Island Adventures importing strings/" .. modulename .. "!\n" .. result)
	else
		return result()
	end
end

local IsTheFrontEnd = rawget(_G, "TheFrontEnd") and rawget(_G, "IsInFrontEnd") and IsInFrontEnd()

if not IsTheFrontEnd then
    -- add character speech
    for _,v in pairs(speech) do
    	merge(STRINGS.CHARACTERS[string.upper(v)], import(v .. ".lua"))
    end
    for _,v in pairs(newspeech) do
    	STRINGS.CHARACTERS[string.upper(v)] = import(v .. ".lua")
    end
    for _,v in pairs(pigstuff) do
    	merge(STRINGS, import(v .. ".lua"))
    end
end

for _,v in pairs(other) do
	merge(STRINGS, import(v .. ".lua"))
end
