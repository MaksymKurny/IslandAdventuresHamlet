return MakeInteriorTexturePackage("interior_floor_marble", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_marble.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_check", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_checker.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_check2", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_checkered.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_plaid_tile", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_cityhall.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_sheet_metal", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_sheetmetal.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_wood", INTERIORFACING.FLOOR, "levels/textures/interiors/noise_woodfloor.tex", "wood"),

		MakeInteriorTexturePackage("interior_wall_wood", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_woodwall.tex"),
		MakeInteriorTexturePackage("interior_wall_checkered", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_checkered_metal.tex"),
		MakeInteriorTexturePackage("interior_wall_floral", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_floraltrim2.tex"),
		MakeInteriorTexturePackage("interior_wall_sunflower", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_sunflower.tex"),        
		MakeInteriorTexturePackage("interior_wall_harlequin", INTERIORFACING.WALL, "levels/textures/interiors/harlequin_panel.tex"),

--
		MakeInteriorTexturePackage("interior_floor_gardenstone", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_gardenstone.tex", "dirt"),
		MakeInteriorTexturePackage("interior_floor_geometrictiles", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_geometrictiles.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_shag_carpet", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_shag_carpet.tex", "carpet"),
		MakeInteriorTexturePackage("interior_floor_transitional", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_transitional.tex", "wood"), --CHECK
		MakeInteriorTexturePackage("interior_floor_woodpanels", INTERIORFACING.FLOOR, "levels/textures/interiors/floor_woodpanels.tex", "wood"),
		MakeInteriorTexturePackage("interior_floor_herringbone", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_herringbone.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_hexagon", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_hexagon.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_hoof_curvy", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_hoof_curvy.tex", "marble"),
		MakeInteriorTexturePackage("interior_floor_octagon", INTERIORFACING.FLOOR, "levels/textures/interiors/shop_floor_octagon.tex", "marble"),

		MakeInteriorTexturePackage("interior_wall_peagawk", INTERIORFACING.WALL, "levels/textures/interiors/wall_peagawk.tex"),
		MakeInteriorTexturePackage("interior_wall_plain_ds", INTERIORFACING.WALL, "levels/textures/interiors/wall_plain_DS.tex"),
		MakeInteriorTexturePackage("interior_wall_plain_rog", INTERIORFACING.WALL, "levels/textures/interiors/wall_plain_RoG.tex"),
		MakeInteriorTexturePackage("interior_wall_rope", INTERIORFACING.WALL, "levels/textures/interiors/wall_rope.tex"),
		MakeInteriorTexturePackage("interior_wall_circles", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_circles.tex"),
		MakeInteriorTexturePackage("interior_wall_marble", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_marble.tex"),
		MakeInteriorTexturePackage("interior_wall_mayorsoffice", INTERIORFACING.WALL, "levels/textures/interiors/wall_mayorsoffice_whispy.tex"),
		MakeInteriorTexturePackage("interior_wall_fullwall_moulding", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_fullwall_moulding.tex"),
		MakeInteriorTexturePackage("interior_wall_upholstered", INTERIORFACING.WALL, "levels/textures/interiors/shop_wall_upholstered.tex")
